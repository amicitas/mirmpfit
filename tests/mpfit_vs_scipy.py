# ========================================================================== #

from __future__ import absolute_import, with_statement, absolute_import, \
                       division, print_function, unicode_literals

# ========================================================================== #
# ========================================================================== #
"""
Compare speed and fit quality for a few cases using mpfit and scipy.optimize.leastsq
"""
try:
    from mpfit import mpfit
except:
    from ..mpfit import mpfit
# end try
try:
    from mpfit.tests.timing import print_timing
except:
    from .timing import print_timing
# end try

from scipy.optimize import leastsq
import scipy.optimize
import timeit
import numpy as _np
import matplotlib.pyplot as _plt

def gaussian(x,A,dx,w, return_components=False):
    """
    Returns a 1-dimensional gaussian of form
    H+A*numpy.exp(-(x-dx)**2/(2*w**2))

    [height,amplitude,center,width]

    return_components does nothing but is required by all fitters

    """
    x = _np.array(x) # make sure xarr is no longer a spectroscopic axis
    return A*_np.exp(-(x-dx)**2/(2.0*w**2))

# def analyticGaussian(x, A, dx, w, return_components=False):
#     from FIT import model_spec as _ms
#     x = _np.array(x)
#     return _ms.gaussian(x, [A, dx, w])

# def analyticJacobian(x, A, dx, w, return_components=False):
#     """
#     -->  All using the analytic fitting module: external calls are slower
#     """
#     from FIT import model_spec as _ms
#     x = _np.array(x)
#     return _ms.partial_gaussian(x, [A, dx, w])

def analyticJacobian(XX, AA, x0, ss, **kwargs):
    """
    -->  All local calls: scales ~identically to mpfitter
    Jacobian of a Gaussian
    f = A*exp(-(x-xo)**2.0/(2.0*ss**2.0))
        A = af[0]
        xo = af[1]
        ss = af[2]

    dfdx = -(x-xo)/ss^2 *f
    d2fdx2 = -1/ss^2*f - (x-xo)/ss^2*dfdx

    dfdA = f/A
    dfdxo = A*(x-xo)*exp(-(x-xo)**2.0/(2.0*ss**2.0))/ss^2
          = (x-xo)/ss^2 * f
    dfdss =  (x-xo)^2/ss^3 * f
    """
    XX = _np.array(XX)
    prof = gaussian(XX, AA, x0, ss, **kwargs)
    gvec = _np.zeros( (3,_np.size(XX)), dtype=_np.float64)
    gvec[0,:] = _np.divide(prof, AA)
    gvec[1,:] = _np.divide(XX-x0, ss*ss)*prof
    gvec[2,:] = ((XX-x0)*_np.divide(XX-x0, ss*ss*ss)) * prof
    return gvec

def error_function_generator(xdata, ydata, error=None, model=gaussian,
        mpfit=False, lmfit=False, sumsquares=False, jacout=False, **kwargs):
    """
    Return a function that generates the errors given input parameters
    """
    status = 0
    if error is None:
        error = _np.ones(ydata.shape)

    def error_function(params, **kwargs):
        if lmfit:
            params = [p.value for p in params.values()]
        err = (ydata - model(xdata, *params)) / error
        if sumsquares:
            return (err**2).sum()
        elif mpfit and not jacout:
            return {'status':status, 'residual':err}
        elif mpfit and jacout:
            # analytic jacobian of the residual function -> this step likely is slow
            fjac = -analyticJacobian(xdata, *params)/(_np.ones( (len(params),1))*_np.atleast_2d(error))
            return {'status':status, 'residual':err, 'jacobian':fjac}
        else:
            return err
    return error_function

def mpfitter(xdata, ydata, params=None, error=None, model=gaussian, quiet=True, **kwargs):
    """
    Find the least-squares fit using mpfit
    """

    errfunc = error_function_generator(xdata,ydata,error=error,model=model,mpfit=True)

    mp = mpfit(errfunc, params, quiet=quiet, **kwargs)

    return mp.params,mp.perror

def mpfitterAnal(xdata, ydata, params=None, error=None, model=gaussian, quiet=True, **kwargs):
    """
    Find the least-squares fit using mpfit
    """

    errfunc = error_function_generator(xdata,ydata,error=error,model=model,mpfit=True, jacout=True)

    mp = mpfit(errfunc, params, quiet=quiet, **kwargs)

    return mp.params,mp.perror

def lsfitter(xdata, ydata, params=None, error=None, model=gaussian):
    """
    Find the least-squares fit using scipy.optimize.leastsq
    """

    errfunc = error_function_generator(xdata,ydata,error=error,model=model)

    p, cov, infodict, errmsg, success = leastsq(errfunc, params, full_output=1)

    if not success or cov is None:
        print('leastsq failed epically')
    try:
        return p, cov.diagonal()**0.5
    except:
        print('cov is None at output of leastsq')
        return p, None
    # end try

def annealfitter(xdata, ydata, params=None, error=None, model=gaussian):
    """
    if scipy is less than version 0.16
        Find the fit using scipy.optimize.anneal
    otherwise use basinhopping .... they deprecated anneal ... annoyingly
    """

    errfunc = error_function_generator(xdata,ydata,error=error,model=model, sumsquares=True)

    if float(scipy.__version__.replace('.',''))<16-1e-10:
        p = scipy.optimize.anneal(errfunc, params, full_output=1)
    else:
        p = scipy.optimize.basinhopping(errfunc, params)  # super slow compared to the others
        # success = p.success
        # errmsg = p.message
        # status = p.status
        p = p.x
    # end if
    return p

def fminfitter(xdata, ydata, params=None, error=None, model=gaussian, disp=False):
    """
    Find the fit using scipy.optimize.fmin
    """

    errfunc = error_function_generator(xdata,ydata,error=error,model=model, sumsquares=True)

    p = scipy.optimize.fmin(errfunc, params, full_output=1, disp=disp)

    return p

def lmfitter(xdata, ydata, params=None, error=None, model=gaussian):
    import lmfit

    errfunc = error_function_generator(xdata,ydata,error=error,model=model,lmfit=True)

    parin = lmfit.Parameters()
    parin.add('amplitude',value=params[0])
    parin.add('shift',value=params[1])
    parin.add('width',value=params[2])

    result = lmfit.minimize(errfunc, parin)
    return [r.value for r in parin.values()],[r.stderr for r in parin.values()]

if __name__ == "__main__":
    #do some timing
    length = 1000
    xarr = _np.linspace(-5,5,length)
    yarr = gaussian(xarr, 0.75, -0.25, 2.2)
    noise = _np.random.randn(length) * 0.25
    err = _np.ones(length)*0.25

    print(mpfitter(xarr,yarr+noise,[1,0,1],err))
    print(lsfitter(xarr,yarr+noise,[1,0,1],err))
    print(fminfitter(xarr,yarr+noise,[1,0,1],err))
    # print(lmfitter(xarr,yarr+noise,[1,0,1],err))
    # print(annealfitter(xarr,yarr+noise,[1,0,1],err))
    print(mpfitterAnal(xarr,yarr+noise,[1,0,1],err))

    # function_names = ['mpfitter','lsfitter','fminfitter']
    function_names = ['mpfitter']
    function_names.append('lsfitter')
    function_names.append('fminfitter')
    # function_names.append('lmfitter')
    # function_names.append('annealfitter')
    function_names.append('mpfitterAnal')
    # function_names = ['mpfitter','lsfitter','fminfitter','lmfitter','annealfitter']
    # function_names = ['mpfitter','lsfitter','fminfitter','annealfitter']

    A,dx,s,n = 0.75,-0.25,2.2,0.25

    nfits = 25
    ntries = 7

    print("%18s"%("nelements",)+"".join(["%18s" % fn for fn in function_names]))
    mins = {}
    nels = (2e1,5e1,1e2,2e2,3e2,4e2,5e2,7.5e2,1e3,2.5e3,5e3,1e4,5e4,1e5)
    # nels = (2e1,5e1,1e2,2e2,3e2,4e2,5e2,7.5e2,1e3,2.5e3,5e3,1e4,5e4,1e5,5e5)
    # nels = (2e1,5e1)
    for nelements in nels:
        min_i = ["%18f" % (min(timeit.Timer("%s(xarr,yarr+noise,[1,0,1],err)" % (fn),
            setup="from mpfit_vs_scipy import %s,gaussian; import numpy as _np; xarr=_np.linspace(-5,5,%i);\
                    yarr=gaussian(xarr,%f,%f,%f); noise=_np.random.randn(%i);\
                    err = _np.ones(%i)*%f" % (fn, nelements, A, dx, s, nelements, nelements, n)).repeat(ntries,nfits)))
            for fn in function_names]
        print("%17i:" % (int(nelements)) + "".join(min_i))
        mins[nelements]=min_i

    from pylab import *
    mpmins = _np.asarray([mins[n][0] for n in nels],dtype='float')
    lsmins = _np.asarray([mins[n][1] for n in nels],dtype='float')
    fmmins = _np.asarray([mins[n][2] for n in nels],dtype='float')
    mpanmins = _np.asarray([mins[n][3] for n in nels],dtype='float')
    # lmfits = _np.asarray([mins[n][3] for n in nels],dtype='float')
    # anfits = _np.asarray([mins[n][4] for n in nels],dtype='float')

    _plt.figure()
    loglog(nels,mpmins,label='mpfit')
    loglog(nels,lsmins,label='leastsq')
    loglog(nels,fmmins,label='fmin')
    # loglog(nels,lmfits,label='lmfit')
    # loglog(nels,anfits,label='anfit')
    loglog(nels,mpanmins,label='mpanal')
    _plt.xlabel("Number of Elements")
    _plt.ylabel("Evaluation Time for %i fits (seconds)" % nfits)
    _plt.legend(loc='best')
    _plt.savefig("comparison_plot.png")

    _plt.figure()
    _plt.semilogx(nels,mpmins/lsmins,label='mpfit/leastsq')
    _plt.semilogx(nels,fmmins/lsmins,label='fmin/leastsq')
    # _plt.semilogx(nels,lmfits/lsmins,label='lmfit/leastsq')
    # _plt.semilogx(nels,anfits/lsmins,label='anfit/leastsq')
    _plt.semilogx(nels,mpanmins/lsmins,label='mpanal/leastsq')
    _plt.xlabel("Number of Elements")
    _plt.ylabel("Ratio to leastsq (which is generally the fastest)")
    _plt.legend(loc='best')
    _plt.savefig("time_ratio_plot.png")