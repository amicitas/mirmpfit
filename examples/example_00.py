

import mirmpfit

import numpy as np
from matplotlib import pyplot

def function(coeff, x):
    y = np.polyval(coeff, x)
    return y


def residual_function(coeff, x=None, data=None, sigma=None):
    yfit = function(coeff, x)
    residual = (data - yfit)/sigma
    return {'residual':residual}


if __name__ == "__main__":

    # Define polynomal coefficents for the data.
    coeff = np.array([30.3, 20.2, 10.1])
    
    # Define our data range.
    x = np.linspace(-1.0, 1.0, 100)
    
    # Generate the base data.
    y = function(coeff, x)
    
    # Add poisson noise.
    data = np.random.poisson(y,(1,len(y)))[0]
    

    
    # Now start the process of fitting the data.

    # Make the approximation that the error in the data is gaussian.
    sigma = np.sqrt(data)
    
    # Create a dictionary that will be sent to the residual function.
    residual_keywords = {'x':x, 'data':data, 'sigma':sigma}
    
    # Make and initial guess of the parameters.
    param_guess = np.array([10.0, 10.0, 10.0])
    
    # Perform the actual fit.
    mp = mirmpfit.mpfit(residual_function
                       ,param_guess
                       ,residual_keywords=residual_keywords
                       ,quiet=True)
    
    # Get the final function value from the fit.
    yfit = function(mp.params, x)
    
    
    
    # Print the fit results.
    print('Results:')
    print('{:>5s}  {:>6s}  {:>6s}     {:>4s}'.format('param', 'actual', 'fit', 'error'))
    for ii in range(len(coeff)):
        print('{:>5d}  {:>6.2f}  {:>6.2f} +/- {:>4.2f}'.format(ii, coeff[ii], mp.params[ii], mp.perror[ii]))
    
    # Plot the fit results
    pyplot.close()
    pyplot.scatter(x,data)
    pyplot.plot(x, yfit, color='red')
    pyplot.show()
