#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================

"""
author
------
  Novimir Antoniuk Pablant
    - novimir.pablant@amicitas.com

description
-----------
  An mpfit example that utilizes a generalized model object that automatically
  translates between a parameter dictionary and the parameter array used by
  mpfit.  The use of this type of object makes the fitting model very
  flexible and easy to develop or modify.

"""


from collections import OrderedDict
import scipy.interpolate
import numpy as np
from matplotlib import pyplot


import mpfit as mirmpfit

class ModelSpline():

    def __init__(self, data=None):
        self.param = None
        self.sigma = None
        self.parinfo = None
        self.options = self.defaultOptions()
        self.set_data(data)


    def defaultOptions(self):
        options = OrderedDict()
        options['fix_all'] = False
        options['fix_knots_x'] = False
        options['fix_knots_y'] = False
        options['num_knots'] = 5

        return options


    def set_data(self, data):
        self.data = data


    def set_options(self, user_options):
        if user_options is None:
            return
        for key in self.options:
            if key in user_options:
                self.options[key] = options[key]


    def get_spline_object(self, spline_type):

        if spline_type == 'linear':
            spline = scipy.interpolate.InterpolatedUnivariateSpline(
                self.param['knots_x']
                ,self.param['knots_y']
                ,k=1)
        elif spline_type == 'cubic':
            spline = scipy.interpolate.CubicSpline(
                self.param['knots_x']
                ,self.param['knots_y']
                )
        elif spline_type == 'pchip':
            spline = scipy.interpolate.PchipInterpolator(
                self.param['knots_x']
                ,self.param['knots_y']
                )

        return spline


    def evaluate(self, param_array=None):
        if param_array is not None:
            self.extract_param_array(param_array)

        spline_type = 'pchip'

        spline = self.get_spline_object(spline_type)

        y = spline(self.data['x'])
        return y


    def evaluate_residual(self, param_array=None):
        yfit = self.evaluate(param_array)
        residual = (self.data['y'] - yfit)/self.data['sigma']
        return residual


    def evaluate_chisq(self, param_array=None):
        residual = self.evaluate_residual(param_array)
        chisq = np.sum(residual**2)
        return chisq


    def initialize(self):

        # Create a dictionary of params.
        self.param = OrderedDict()
        self.param['knots_x'] = np.linspace(-1.0, 1.0, self.options['num_knots'])
        self.param['knots_y'] = np.zeros(self.options['num_knots'])+0.1

        # Create a dictionary of sigmas.
        self.sigma = OrderedDict()
        for key in self.param:
            self.sigma[key] = np.zeros(len(self.param[key]))

        # create a dictionary of parinfos
        self.parinfo = OrderedDict()
        for key in self.param:
            self.parinfo[key] = np.array([{} for xx in range(len(self.param[key]))])

        for ii in range(self.options['num_knots']):
            self.parinfo['knots_x'][ii]['fixed'] = False
            self.parinfo['knots_x'][ii]['limited'] = [False, False]
            self.parinfo['knots_x'][ii]['limits'] = [0.0, 0.0]
            self.parinfo['knots_x'][ii]['step'] = 0.0001

        # Fix the x-location of the end knots.
        self.parinfo['knots_x'][0]['fixed'] = True
        self.parinfo['knots_x'][-1]['fixed'] = True

        # Limit each knot to a certain range of values.
        # This also ensures that the knots can't cross.
        for ii in range(1,self.options['num_knots']-1):
            self.parinfo['knots_x'][ii]['limited'] = [True, True]
            self.parinfo['knots_x'][ii]['limits'][0] = (
                (self.param['knots_x'][ii-1]+self.param['knots_x'][ii])/2)
            self.parinfo['knots_x'][ii]['limits'][1] = (
                (self.param['knots_x'][ii]+self.param['knots_x'][ii+1])/2)

        for ii in range(self.options['num_knots']):
            self.parinfo['knots_y'][ii]['fixed'] = False
            self.parinfo['knots_y'][ii]['limited'] = [True, False]
            self.parinfo['knots_y'][ii]['limits'] = [0.005, 0.0]
            self.parinfo['knots_y'][ii]['step'] = 0.0001

        if self.options['fix_knots_x'] or self.options['fix_all']:
            for parinfo in self.parinfo['knots_x']:
                parinfo['fixed'] = True

        if self.options['fix_knots_y'] or self.options['fix_all']:
            for parinfo in self.parinfo['knots_y']:
                parinfo['fixed'] = True


    def extract_param_array(self, param_array):
        index = 0
        for key in self.param:
            num_val = len(self.param[key])
            self.param[key][:] = param_array[index:index+num_val]
            index += num_val


    def extract_sigma_array(self, sigma_array):
        index = 0
        for key in self.sigma:
            num_val = len(self.param[key])
            self.sigma[key][:] = sigma_array[index:index+num_val]
            index += num_val


    def get_param_array(self):
        total_val = 0
        for key in self.param:
            total_val += len(self.param[key])
        output = np.empty(total_val)

        index = 0
        for key in self.param:
            num_val = len(self.param[key])
            output[index:index+num_val] = self.param[key]
            index += num_val

        return output


    def get_parinfo_array(self):
        total_val = 0
        for key in self.parinfo:
            total_val += len(self.parinfo[key])
        output = np.empty(total_val, dtype='O')

        index = 0
        for key in self.parinfo:
            num_val = len(self.parinfo[key])
            output[index:index+num_val] = self.parinfo[key]
            index += num_val

        return output


    def print_results(self):
        for key in self.param:
            print('{}:'.format(key))
            for ii in range(len(self.param[key])):
                print('  {:02d} | {:6.2f}+/-{:6.2f}'.format(
                    ii
                    ,self.param[key][ii]
                    ,self.sigma[key][ii]))


    def plot_results(self):
        """
        Plot the data along with the current model state.
        """

        yfit = self.evaluate()

        # Plot the fit results
        pyplot.close()
        pyplot.scatter(self.data['x'], self.data['y'])
        pyplot.plot(self.data['x'], yfit, color='red')

        pyplot.errorbar(
            self.param['knots_x']
            ,self.param['knots_y']
            ,yerr=self.sigma['knots_y']
            ,xerr=self.sigma['knots_x']
            ,linestyle=''
            ,marker='.'
            ,markersize=10.0
            ,color='red')
        pyplot.show()


class Example02():

    def get_rawdata(self):

        # Define polynomal coefficents for the data.
        coeff = np.array([30.3, 20.2, 10.1])

        # Define our data range.
        x = np.linspace(-1.0, 1.0, 100)

        # Generate the base data.
        y = np.polyval(coeff, x)

        # Add poisson noise.
        data = np.random.poisson(y,(1,len(y)))[0]

        output = {}
        output['x'] = x
        output['y'] = data
        output['coeff'] = coeff

        return output

    def get_sigma(self, data):

        # Assume that the data is pure poisson distribution
        # and approximate the error as sqrt(signal).

        sigma = np.sqrt(data['y'])

        return sigma

    def get_data(self):
        data = self.get_rawdata()
        data['sigma'] = self.get_sigma(data)

        return data


    def run(self, options=None):
        """
        Run the fitting example.

        This will generate a data set, fit the data set, and display the results.
        """

        self.model = ModelSpline()
        self.data = self.get_data()
        self.model.set_data(self.data)
        self.model.set_options(options)

        self.results = self.fit()

        self.model.print_results()
        self.model.plot_results()


    def fit(self):

        self.model.initialize()
        param_guess = self.model.get_param_array()
        parinfo = self.model.get_parinfo_array()

        # Perform the actual fit.
        mp = mirmpfit.mpfit(self.model.evaluate_residual
                            ,param_guess
                            ,parinfo=parinfo
                            ,quiet=True)
        self.mp = mp

        # Set the final params and errors within the model.
        self.model.extract_param_array(mp.params)
        if mp.status > 0:
            self.model.extract_sigma_array(mp.perror)

        # Get the final function value from the fit.
        yfit = self.model.evaluate()
        chisq = self.model.evaluate_chisq()

        # Create an output dictionary.
        output = {'yfit':yfit
                  ,'params':mp.params
                  ,'perror':mp.perror
                  ,'chisq':chisq}


        print(mp.statusString())
        print(mp.errmsg)
        print('        Chisq: {:10.3f}'.format(output['chisq']))
        if mp.dof > 0:
            print('Reduced Chisq: {:10.3f}'.format(output['chisq']/mp.dof))

        return output


if __name__ == "__main__":

    obj = Example02()
    obj.run()
