

import mirmpfit

import numpy as np
from matplotlib import pyplot

class Example01():

    def __init__(self):
        self.x = None
        self.data = None
        self.sigma = None

        
    def setup_data(self):
        # Define polynomal coefficents for the data.
        coeff = np.array([30.3, 20.2, 10.1])

        # Define our data range.
        x = np.linspace(-1.0, 1.0, 100)

        # Generate the base data.
        y = function(coeff, x)

        # Add poisson noise.
        data = np.random.poisson(y,(1,len(y)))[0]



        # Now start the process of fitting the data.

        # Make the approximation that the error in the data is gaussian.
        sigma = np.sqrt(data)

        # Save the data as part of the object.
        self.coeff = coeff
        self.x = x
        self.data = data
        self.sigma = sigma

        
    def function(self, coeff, x):
        y = np.polyval(coeff, x)
        return y


    def residual_function(self, coeff):
        yfit = self.function(coeff, self.x)
        residual = (self.data - yfit)/self.sigma
        return {'residual':residual}


    def fit(self):

        # Make and initial guess of the parameters.
        param_guess = np.array([10.0, 10.0, 10.0])

        # Perform the actual fit.
        mp = mirmpfit.mpfit(self.residual_function
                           ,param_guess
                           ,quiet=True)

        # Get the final function value from the fit.
        yfit = function(mp.params, x)

        # Create an output dictionary.
        output = {'yfit':yfit
                  ,'params':mp.params
                  ,'perror':mp.perror}

        return output

    
    def run(self):
        """
        Run the fitting example. 

        This will generate a data set, fit the data set, and display the results.
        """
        
        self.setup_data()
        results = self.fit()

        self.print_results(results)
        self.plot_results(results)

        
    def print_results(self, results):
        print('Results:')
        print('{:>5s}  {:>6s}  {:>6s}     {:>4s}'.format(
            'param'
            ,'actual'
            ,'fit'
            ,'error'))
        
        for ii in range(len(self.coeff)):
            print('{:>5d}  {:>6.2f}  {:>6.2f} +/- {:>4.2f}'.format(
                ii
                ,coeff[ii]
                ,results['params'][ii]
                , results['perror'][ii]))

            
    def plot_results(self, results):
        # Plot the fit results
        pyplot.close()
        pyplot.scatter(self.x, self.data)
        pyplot.plot(self.x, results['yfit'], color='red')
        pyplot.show()

    
if __name__ == "__main__":

    obj = Example01()
    obj.run()
